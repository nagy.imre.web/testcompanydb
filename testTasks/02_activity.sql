SET @sql = NULL;
SELECT GROUP_CONCAT(
        DISTINCT CONCAT(
            'max(case when activity = ''',
            activity,
            ''' then companyName end) "',
            activity,
            '"'
        )
    ) INTO @sql
FROM companies.testcompanydb;
SET @sql = CONCAT(
        'SELECT ',
        @sql,
        ' FROM companies.testcompanydb GROUP BY companyID'
    );
-- SELECT @SQL;
PREPARE stmt
FROM @sql;
EXECUTE stmt;
-- DEALLOCATE PREPARE stmt;
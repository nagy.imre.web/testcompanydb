DELIMITER $$
CREATE DEFINER = `companies` @`localhost` TRIGGER `trigger_testcompanydb_before_update`
BEFORE UPDATE ON `testcompanydb`
FOR EACH ROW
BEGIN
    IF NEW.companyFoundationDate <> old.companyFoundationDate THEN
        SET new.companyFoundationDate = old.companyFoundationDate;
    END IF;
END $$
WITH recursive Date_Ranges AS (
    SELECT '2001-01-01' as Date
    UNION ALL
    SELECT Date + interval 1 day
    FROM Date_Ranges
    where Date < DATE(NOW())
)
SELECT Date_Ranges.Date,
    t.companyName
FROM Date_Ranges
    LEFT JOIN testcompanydb t ON Date_Ranges.Date = t.companyFoundationDate;
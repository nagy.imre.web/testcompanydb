<?php

namespace Companies\Main\Models;

use CodeIgniter\Model;
use InvalidArgumentException;
use UnexpectedValueException;

class Companies extends Model
{
    protected $DBGroup = 'default';
    protected $table      = 'testcompanydb';
    protected $primaryKey = 'companyId';

    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['companyName', 'companyRegistrationNumber', 'companyFoundationDate', 'country', 'zipCode', 'city', 'streetAddress', 'latitude', 'longitude', 'companyOwner', 'employees', 'activity', 'email'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    /**
     * addCompany
     *
     * @param  mixed $companyData
     * @return bool
     */
    public function addCompany(array $companyData): bool
    {
        if (empty($companyData)) {
            throw new InvalidArgumentException('Empty $companyData');
        }
        $insertCols = [];
        $insertValues = [];
        foreach ($this->allowedFields as $field) {
            if (array_key_exists($field, $companyData)) {
                $insertCols[] = $field;
                $insertValues[] = $companyData[$field];
            }
        }
        if (empty($insertCols) || count($insertCols) !== count($insertValues)) {
            throw new InvalidArgumentException('empty insertCols or countOfInsertCols != countOfInsertValues');
        }
        $sql = "INSERT INTO " . $this->table . " (" . implode(",", $insertCols) . ") VALUES (" . $this->bindPlaceholder($insertValues) . ")";
        $result = $this->db->query($sql, $insertValues);
        return ($result === true);
    }


    /**
     * updateCompany
     *
     * @param  array $companyIds
     * @return bool
     */
    public function updateCompany(array $companyDatas, int $companyId): bool
    {
        if (empty($companyId)) {
            throw new InvalidArgumentException('Empty companyId');
        }
        if (empty($companyDatas)) {
            throw new InvalidArgumentException('Empty companyDatas array');
        }

        $updateArray = [];
        foreach ($this->allowedFields as $allowedField) {
            if (array_key_exists($allowedField, $companyDatas)) {
                $updateArray[$allowedField] = $companyDatas[$allowedField];
            }
        }
        
        if (empty($updateArray)) {
            throw new InvalidArgumentException('Array companyDatas: doesn\'t contain valid update key');
        }

        $set = [];
        $values = [];
        foreach ($updateArray as $updateKey => $updateItem) {
            $set[] = $updateKey . '= ?';
            $values[] = $updateItem;
        }
        $values[] = $companyId;

        $sql = "UPDATE " . $this->table . " SET " . implode(', ', $set) . " WHERE companyId = ? LIMIT 1";

        $result = $this->db->query($sql, $values);
        if ($result === False) {
            throw new UnexpectedValueException('Failed query or empty result');
        }
        return $result;
    }

    /**
     * getCompany
     *
     * @param  array $companyIds
     * @return array
     */
    public function getCompany(array $companyIds): array
    {
        if (empty($companyIds)) {
            throw new InvalidArgumentException('Empty companyIds array');
        }

        $where = "companyId in (" . $this->bindPlaceholder($companyIds) . ")";
        if (count($companyIds) === 1) {
            $where = "companyId = ?";
        }
        $sql = "SELECT * from " . $this->table . " WHERE " . $where;
        $result = $this->db->query($sql, $companyIds);
        if ($result === False || $result->getNumRows() < 1) {
            return [];
        }
        $resultArray = $result->getResultArray();
        return $resultArray;
    }

    /**
     * removeCompany
     *
     * @param  int $companyId
     * @return bool
     */
    public function removeCompany(int $companyId): bool
    {
        if (empty($companyId)) {
            throw new InvalidArgumentException('Empty companyId');
        }
        $sql = "DELETE FROM " . $this->table . " WHERE companyId = ? LIMIT 1";
        $result = $this->db->query($sql, $companyId);
        return ($result !== False);
    }

    /**
     * bindPlaceholder
     *
     * @param  array $colsOrValues
     * @return string
     */
    protected function bindPlaceholder(array $colsOrValues): string
    {
        if (empty($colsOrValues)) {
            throw new InvalidArgumentException('Empty colsOrValues array');
        }
        return implode(', ', array_fill(0, count($colsOrValues), '?'));
    }
}

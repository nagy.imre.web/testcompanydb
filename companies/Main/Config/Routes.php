<?php

namespace Companies\Main\Config;

$routes->group('', ['namespace' => 'Companies\Main\Controllers'], function ($routes) {
    $routes->add('/', 'Companies::index');
});

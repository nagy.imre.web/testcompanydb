<?php

namespace Companies\Main\Controllers;

use App\Controllers\BaseController;
use Companies\Main\Models;

class Companies extends BaseController
{
    public function index()
    {
        $model = model('Companies');
        var_dump("Nothing to see - copany does not exist (yet)");
        var_dump($model->getCompany([321]));
        var_dump("Select one company");
        var_dump($model->getCompany([60]));
        var_dump("Select two companies");
        var_dump($model->getCompany([61, 91]));
        var_dump("Insert a company");
        $insertArray = [
            'companyName' => 'OneOfThem',
            'companyRegistrationNumber' => 'HU98531458',
            'companyFoundationDate' => '2011-05-09',
            'country' => 'Hungary',
            'zipCode' => 'H2154',
            'city' => 'Vatican City',
            'streetAddress' => 'St.Peter Street 1',
            'latitude' => '0.01',
            'longitude' => '-0.01',
            'companyOwner' => 'MrNoname',
            'employees' => '1230',
            'activity' => 'meditation',
            'email' => 'best@onecompany.com'];
        var_dump($model->addCompany($insertArray));
        var_dump("Select two new companies id(101,102)");
        var_dump($model->getCompany([101,102]));
        var_dump("Update a company");
        $updateArray = [
            'companyName' => 'OneOfThem',
            'companyRegistrationNumber' => 'HU98531458',
            'companyFoundationDate' => '2011-06-19'];
        var_dump($model->updateCompany($updateArray,101));
        var_dump("Select two new companies id(101,102)");
        var_dump($model->getCompany([101,102]));

        var_dump("Delete a company id(101)");
        var_dump($model->updateCompany($updateArray,206));
        var_dump("Select two new companies id(101,102)");
        var_dump($model->getCompany([101,102]));

        return "";
    }
}
